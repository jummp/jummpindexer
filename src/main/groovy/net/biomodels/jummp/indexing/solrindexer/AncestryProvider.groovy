/**
 * Copyright (C) 2010-2015 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing.solrindexer
import net.biomodels.jummp.annotationstore.ResourceReference
/**
 * Interface for fetching the parents of an annotation term found in a model.
 *
 * Aging is a kind of biological process. If the latter was defined in an ontology
 * to be the parent of the former, then implementations of this interface would
 * be required to record this information if they cover the ontology that provided
 * this relationship.
 *
 * The ancestry obtained for the same term may vary from one implementation to another.
 *
 * @author Mihai Glonț <mglont@ebi.ac.uk>
 */
interface AncestryProvider {
    /**
     * Indicates whether an implementation can provide ancestry information
     * @return true if parent terms can be extracted, false otherwise.
     */
    boolean supportsAncestry()
    /**
     * Records all parents for a given annotation term
     * @param reference the annotation term for which to retrieve ancestry.
     */
    void getAncestors(ResourceReference reference)
}
