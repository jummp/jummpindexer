/**
 * Copyright (C) 2010-2015 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing.solrindexer

import net.biomodels.jummp.indexing.solrindexer.miriam.MiriamRegistryService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import net.biomodels.jummp.annotationstore.ResourceReference
/**
 *
 * @author raza
 */
class AnnotationReferenceResolver {
    private static final Logger logger = LoggerFactory.getLogger(AnnotationReferenceResolver.class)
    static enum AnnoType {MIRIAM, IDENTIFIER, UNKNOWN}

    private static AnnotationReferenceResolver _instance

    def onlineProcessors = []
    def offlineProcessors = []

    boolean isOnline = false

    public void setOnline(boolean value) {
        isOnline = value
    }

    public AnnotationReferenceResolver() {
    }

    public AnnotationReferenceResolver(Properties jummpProperties) {
        if (jummpProperties.containsKey("jummp.indexer.ols.dumpAvailable")) {
            String value = jummpProperties.getProperty("jummp.indexer.ols.dumpAvailable")
            if (value == "true") {
                isOnline = false
                String host = jummpProperties.getProperty("jummp.indexer.ols.dump.host")
                String port = jummpProperties.getProperty("jummp.indexer.ols.dump.port")
                String db = jummpProperties.getProperty("jummp.indexer.ols.dump.db")
                String username = jummpProperties.getProperty("jummp.indexer.ols.dump.username")
                String password = jummpProperties.getProperty("jummp.indexer.ols.dump.password")
                offlineProcessors.add(new OLSDumpQuerier(host,
                                                         port,
                                                         db,
                                                         username,
                                                         password))
            }
        }
    }

    public static void initialiseWithProperties(String jummpFile) {
        Properties jummpProperties = new Properties()
        jummpProperties.load(new FileInputStream(jummpFile))
        _instance = new AnnotationReferenceResolver(jummpProperties)
    }

    public static AnnotationReferenceResolver instance() {
        if (!_instance) {
            _instance = new AnnotationReferenceResolver()
        }
        return _instance
    }

    public void shutdown() {
        onlineProcessors.each {
            it.shutdown()
        }
        offlineProcessors.each {
            it.shutdown()
        }
    }

    public AnnoType getType(String url) {
        if (url.contains("miriam:")) {
            return AnnoType.MIRIAM
        }
        if (url.contains("http://identifiers.org/")) {
            return AnnoType.IDENTIFIER
        }
        return AnnoType.UNKNOWN
    }

    public String[] extractParts(String url, AnnoType type) {
        String[] retval
        switch(type) {
            case AnnoType.MIRIAM:
                retval = extractMiriamParts(url)
                break;
            case AnnoType.IDENTIFIER:
                retval = extractIdentifierParts(url)
        }
        return retval;
    }

    public boolean isSupported(String collection) {
        if (isOnline) {
            return onlineProcessors.find { it.supportsCollection(collection) } != null
        }
        return offlineProcessors.find { it.supportsCollection(collection) } != null
    }

    public ResourceReference resolve(String id, String type) {
        AncestryProvider ancestryProvider = getAncestryProvider(type)
        SynonymProvider synonymProvider = getSynonymProvider(type)
        TermInformationProvider termInformationProvider = getTermInformationProvider(type)
        AnnotationReference ref = AnnotationReference.constructAnnotationReference(
            ancestryProvider, termInformationProvider, synonymProvider, id, type)
        return ref.getResource()
    }

    // At the moment neither of AncestryProvider/SynonymProvider have a way of specifying
    // whether they support this functionality for a particular collection. Would be needed
    // to decouple with TermInfoProvider

    private AncestryProvider getAncestryProvider(String collection) {
        if (isOnline) {
            return onlineProcessors.find {
                it instanceof AncestryProvider && it.supportsCollection(collection)
            }
        }
        return offlineProcessors.find {
            it instanceof AncestryProvider && it.supportsCollection(collection)
        }
    }

    private SynonymProvider getSynonymProvider(String collection) {
        if (isOnline) {
            return onlineProcessors.find {
                it instanceof SynonymProvider && it.supportsCollection(collection)
            }
        }
        return offlineProcessors.find {
            it instanceof SynonymProvider && it.supportsCollection(collection)
        }
    }

    private TermInformationProvider getTermInformationProvider(String collection) {
        if (isOnline) {
            return onlineProcessors.find {
                it instanceof TermInformationProvider && it.supportsCollection(collection)
            }
        }
        return offlineProcessors.find {
            it instanceof TermInformationProvider && it.supportsCollection(collection)
        }
    }


    private String[] extractMiriamParts(String url) {
        int colonIndex = url.lastIndexOf(':')
        String datatypeUrn = url.substring(0, colonIndex)
        String identifier = url.substring(colonIndex + 1)
        return [datatypeUrn, identifier] as String[]
    }

    private String[] extractIdentifierParts(String url) {
        String newUrl = MiriamRegistryService.updateURI(url)
        if (newUrl) {
            url = newUrl
        } else {
            logger.error("MiriamWS returned null for $url!")
        }
        url = url.split("http://identifiers.org/")[1]
        return url.split("/");
    }
}

