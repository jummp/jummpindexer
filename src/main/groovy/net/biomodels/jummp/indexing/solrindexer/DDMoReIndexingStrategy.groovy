/**
 * Copyright (C) 2010-2015 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing.solrindexer

import com.hp.hpl.jena.rdf.model.Literal
import com.hp.hpl.jena.rdf.model.Model
import com.hp.hpl.jena.rdf.model.Statement
import net.biomodels.jummp.annotationstore.Qualifier
import net.biomodels.jummp.annotationstore.ResourceReference
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Concrete implementation of the DDMoRe indexing strategy.
 *
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 * @author Sarala Wimalaratne
 */
class DDMoReIndexingStrategy extends IndexingStrategy {
    /**
     * The class logger.
     */
    static final Logger log = LoggerFactory.getLogger(DDMoReIndexingStrategy.class)
    /**
     * Mapping between the local name of the DDMoRe qualifiers and Solr document fields.
     */
    static final Map<String, String> CONTENT_MAPPINGS = [
        "model-field-purpose": "pharmmlTherapeuticArea",
        "model-has-author": "modelAuthors",
        "model-has-correspondent": "modelAuthors",
        "model-has-description": "pharmmlModellingContextDescription",
        "model-has-description-long": "pharmmlLongTechnicalDescription",
        "model-has-description-short": "pharmmlShortDescription",
        "model-has-publication-source-external-id": "pharmmlPublicationSource",
        "model-implementation-conforms-to-literature-controlled" : "pharmmlImplementationConformsToLiterature",
        "model-implementation-source-discrepancies-freetext": "pharmmlImplementationDiscrepancies",
        "model-modelling-question": "pharmmlModelDevelopmentContext",
        "model-origin-of-code-in-literature-controlled" : "pharmmlCodeFromLiterature",
        "model-related-to-disease-or-condition": "disease",
        "model-research-stage": "pharmmlResearchStage",
        "model-tasks-in-scope": "pharmmlTasks",
        "model-type-of-data": "pharmmlTypeOfData"
    ]

    /**
     * Implementation of the contract for processing a submission.
     *
     * @param ctx The RequestContext of the submission that should be indexed.
     */
    void doIndexSubmission(RequestContext ctx) {
        //takes per-schema annotations and populates ctx
        final String fmt = ctx.modelFormat
        switch (fmt) {
            case "PharmML":
                doIndexPharmMLSubmission(ctx)
                break
            case "SBML":
                // still uses the old indexing strategy, defensively throw an exception
                throw new UnsupportedOperationException("")
                break
            default:
                doIndexGenericSubmission(ctx)
                break
        }
    }

    /**
     * Strategy for indexing PharmML submissions.
     *
     * Extracts element descriptions and annotations and populates @p ctx
     * @param ctx the current request's context.
     */
    protected void doIndexPharmMLSubmission(RequestContext ctx) {
        // extract descriptions
        final File pharmMLFile = new File(ctx.mainFilePaths.first())
        List<String> descriptions = PharmMLIndexerUtils.extractElementDescriptions(pharmMLFile)
        if (descriptions) {
            ctx.partialData.put("elementDescription", descriptions)
        }
        // extract metadata file
        List files = ctx.allFilePaths.collect { String p -> new File(p)}
        File metadataFile = PharmMLIndexerUtils.findAnnotationFile(pharmMLFile, files)
        if (!metadataFile) {
            return
        }
        Model metadata = PharmMLIndexerUtils.readModelAnnotations(metadataFile)
        // process annotations
        processAnnotations(metadata, ctx)
    }

    /**
     * Catch-all indexing strategy for non-PharmML submissions.
     *
     * @param c the current request's context.
     */
    protected void doIndexGenericSubmission(RequestContext c) {
        if (log.isDebugEnabled()) {
            final String path = c.indexFile.absolutePath
            log.debug("Looking for annotations inside generic submission $path")
        }
        // find annotation file
        final List<File> allFiles = c.allFilePaths.collect { new File(it) }
        File annoFile = findAnnotationFileForGenericSubmission(allFiles)
        if (!annoFile) {
            if (log.isDebugEnabled()) {
                final String path = c.indexFile.absolutePath
                log.debug("Found no annotations inside generic submission $path")
            }
            return
        }
        // use pharmml lib metadata to parse annotations
        Model metadata = PharmMLIndexerUtils.readModelAnnotations(annoFile)
        // index them
        processAnnotations(metadata, c)
    }

    /**
     * Convenience method for locating the annotation file for a generic DDMoRe submission.
     *
     * @param files the files of the current request.
     * @return the first valid RDF file found in @p files.
     */
    protected File findAnnotationFileForGenericSubmission(List<File> files) {
        files.find { File f ->
            f.name.endsWith('.rdf')  && JummpXmlUtils.containsElement(f, "RDF")
        }
    }

    /**
     * Traverses the supplied RDF model and persists it in the database and the search index.
     *
     * @param annoModel the annotation model -- typically, this would be created by finding
     * the RDF file for the current submission and parsing it with Apache Jena.
     * @param ctx the request context.
     * @return a mapping between SolrDocument fields and their corresponding values as a
     * result of the annotations being processed.
     */
    Map processAnnotations(Model annoModel, RequestContext ctx) {
        def annotations = [:]
        def iterator = annoModel.listStatements()
        while (iterator.hasNext()) {
            Statement stmt = iterator.nextStatement()
            def s = stmt.subject
            def p = stmt.predicate
            String pName = p.localName
            String pURI = p.getURI()
            def o = stmt.object
            if (CONTENT_MAPPINGS.containsKey(pName)) {
                def solrFieldName = CONTENT_MAPPINGS[pName]
                def content = o.literal ?
                    ((Literal) o).getString() :
                    ((com.hp.hpl.jena.rdf.model.Resource) o).getURI()
                if (annotations.containsKey(solrFieldName)) {
                    //append to existing values
                    def existing = annotations[solrFieldName]
                    if (existing instanceof Collection) {
                        existing.addAll content
                    } else if (existing instanceof CharSequence) {
                        log.error "Key $solrFieldName in $annotations should not be a String"
                        def newContent = [existing]
                        newContent.addAll(content)
                        annotations.put(solrFieldName, newContent)
                    }
                } else { // new field
                    def value = content instanceof CharSequence ? [content] : content
                    annotations.put(solrFieldName, value)
                }
                if (content) {
                    Qualifier qual = Qualifier.findByUri(pURI)
                    if (!qual) {
                        qual = new Qualifier(accession:pName, uri: pURI,
                            qualifierType: 'pharmML')
                        if (!qual.save()) {
                            log.error("""\
Unable to save qualifier $pURI in db while indexing $annotations:${qual.errors.allErrors.dump()}
""")
                        }
                    }
                    //indexAncestors(content, ctx.partialData, qual, s.getURI())
                    ResourceReference r = indexResourceReferenceAncestors(content, ctx,
                            solrFieldName, annotations)
                    saveAnnotation(s.getURI(), qual, r, ctx)
                    ctx.partialData.putAll(annotations)
                }
            }
        }
        return annotations
    }
}
