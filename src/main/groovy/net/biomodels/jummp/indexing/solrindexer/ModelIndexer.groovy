/**
 * Copyright (C) 2010-2015 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.orgin/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing.solrindexer

import groovy.transform.CompileStatic

/**
 * Abstraction of the model indexer.
 *
 * In order to allow models in the same format being indexed in different ways, we implemented
 * the bridge design pattern, which separates an abstraction (in our case the model format)
 * from its implementation (i.e. IndexingStrategy implementations).
 *
 * @see ModelIndexerFactory
 * @see IndexingStrategy
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 */
@CompileStatic
interface ModelIndexer {
    /**
     * Contract for extracting content from a submission.
     *
     * @param context the RequestContext of the submission that should be indexed.
     */
    void extractFileContent(RequestContext context)

    /**
     * Implementations of this method are expected to send the supplied data to Solr.
     *
     * @param data mapping between field names and corresponding values
     * @param solrUrl the URL at which Solr is available.
     */
    void indexData(Map<String, List<String>> data, String solrUrl)
}
