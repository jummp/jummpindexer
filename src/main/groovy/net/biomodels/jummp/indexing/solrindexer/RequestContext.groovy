/**
 * Copyright (C) 2010-2015 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing.solrindexer

import groovy.json.JsonSlurper
import net.biomodels.jummp.indexing.solrindexer.miriam.MiriamRegistryService
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Simple class for interacting with a request's context.
 *
 * Effectively, this class parses an index file produced by Jummp and then provides
 * convenience methods for accessing the properties contained within the file.
 *
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 */
class RequestContext {
    /**
     * The class logger.
     */
    private static final Logger log = LoggerFactory.getLogger(RequestContext.class)
    /**
     * Threshold for the logger's verbosity.
     */
    private static final IS_INFO_ENABLED = log.isInfoEnabled()
    /**
     * File containing two types of information:
     *      1) generic model metadata - e.g. file location, model format, authorship
     *      2) application context - e.g. Solr URL, application settings
     */
    File indexFile
    /**
     * The content defined in the index file as produced by JsonSlurper.
     */
    def searchableContent
    /**
     * Generic model metadata.
     */
    Map partialData
    /**
     * The format in which the model is encoded.
     */
    String modelFormat
    /**
     * The folder containing all files associated with a model.
     */
    String folderPath
    /**
     * The paths of the main files of a submission.
     */
    List<String> mainFilePaths
    /**
     * The list of all files from a submission.
     */
    List<String> allFilePaths
    /**
     * The URL of the Solr core.
     */
    String solrServer
    /**
     * Externalised settings file.
     */
    String configFilePath
    /**
     * Path to the full export of the identifiers.org Registry.
     */
    String registryExportPath
    /**
     * The URL of the database in which to store annotations extract from the model.
     */
    String databaseURL
    /**
     * The database username.
     */
    String databaseUsername
    /**
     * The database password.
     */
    String databasePassword
    /**
     * Dictates the indexing strategy to use in order to process the current submission.
     */
    IndexingEnvironment metadataProcessor = IndexingEnvironment.DDMORE

    // prohibit usage of default constructor
    @SuppressWarnings("GroovyUnusedDeclaration")
    private RequestContext() {}

    /**
     * Constructs a RequestContext with values defined in the given @p documentPath.
     */
    RequestContext(String documentPath) {
        indexFile = new File(documentPath)
        if (IS_INFO_ENABLED) {
            String host = System.getProperty("http.proxyHost")
            if (host) {
                StringBuilder proxySettings = new StringBuilder()
                String port = System.getProperty("http.proxyPort")
                proxySettings.append("Using proxy URL ").append(host).append(':').append(port)
                log.info(proxySettings.toString())
            } else {
                log.info("No proxy settings found.")
            }
            log.info "Extracting request context information from ${indexFile.absolutePath}"
        }
        if (indexFile.exists()) {
            def jsonParser = new JsonSlurper()
            searchableContent = jsonParser.parse(new BufferedReader(new FileReader(indexFile)))
            modelFormat = searchableContent.partialData.modelFormat
            solrServer = searchableContent.solrServer
            folderPath = searchableContent.folder
            partialData = searchableContent.partialData
            configFilePath = searchableContent.jummpPropFile
            registryExportPath = searchableContent.miriamExportFile
            if (registryExportPath) {
                MiriamRegistryService.updateMiriamExportPath(registryExportPath)
            }
            mainFilePaths = searchableContent.mainFiles
            allFilePaths = searchableContent.allFiles
            def dbSettings = searchableContent.database
            databaseURL = dbSettings.url
            databaseUsername = dbSettings.username
            databasePassword = dbSettings.password
        } else {
            log.error "Index file ${indexFile.absolutePath} does not exist"
        }
        if (IS_INFO_ENABLED) {
            log.info "Finished extracting request context information from ${indexFile.name}"
        }
    }

    /**
     * Convenience method to detect whether there are any set properties for a request.
     */
    boolean isEmpty() {
        searchableContent == null || searchableContent?.isEmpty()
    }
}
