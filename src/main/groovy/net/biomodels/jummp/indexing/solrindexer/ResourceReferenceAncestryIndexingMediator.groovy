/**
 * Copyright (C) 2010-2015 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing.solrindexer

import net.biomodels.jummp.annotationstore.ResourceReference
import static net.biomodels.jummp.indexing.solrindexer.AnnotationReferenceResolver.AnnoType

/**
 * Simple registry of xref ancestry indexers.
 *
 * This class provides mechanisms to create and launch processes that extract external
 * information about a given annotation.
 *
 * @see ResourceReferenceAncestryIndexer
 *
 * @author Mihai Glonț <mglont@ebi.ac.uk>
 */
class ResourceReferenceAncestryIndexingMediator {
    /**
     * A non-thread-safe cache of ancestryIndexers based on their supported annotation types.
     */
    Map<AnnoType, ResourceReferenceAncestryIndexer> indexers = [:]

    /**
     * Constructs a ResourceReference for the supplied cross reference.
     *
     * The actual ancestry indexer to handle this request is chosen depending on the type of
     * annotation.
     *
     * @param xref the cross reference to extract information about.
     * @param ctx the current request's context.
     * @param fieldName
     * @param thisStatement
     * @return the ResourceReference corresponding to @p xref
     */
    ResourceReference indexAncestors(String xref, RequestContext ctx, String fieldName,
            Map thisStatement) {
        ResourceReferenceAncestryIndexer ancestryIndexer = createAncestryIndexer xref
        ancestryIndexer.indexAncestors(xref, ctx, fieldName, thisStatement)
    }

    /**
     * Convenience method that returns the appropriate ancestry indexer for the given cross
     * reference.
     *
     * @param xref the cross reference to construct the ancestry indexer for.
     * @return the corresponding ancestry indexer for @p xref
     */
    protected ResourceReferenceAncestryIndexer createAncestryIndexer(String xref) {
        AnnoType annoType = AnnotationReferenceResolver.instance().getType(xref)
        getIndexerForType(annoType)
    }

    /**
     * Creates and caches ancestry indexers for a given annotation type.
     *
     * @param t the annotation type for which to fetch the ancestry indexer.
     * @return the ancestry indexer for annotations of type @p t.
     */
    protected ResourceReferenceAncestryIndexer getIndexerForType(AnnoType t) {
        if (!indexers.containsKey(t)) {
            switch (t) {
                case AnnoType.MIRIAM:
                case AnnoType.IDENTIFIER:
                    indexers.put(t, new MIRIAMResourceReferenceAncestryIndexer())
                    break
                case AnnoType.UNKNOWN:
                    indexers.put(t, new SimpleResourceReferenceAncestryIndexer())
                    break
            }
        }
        return indexers[t]
    }
}
