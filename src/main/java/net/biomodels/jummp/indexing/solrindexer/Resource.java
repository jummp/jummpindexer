package net.biomodels.jummp.indexing.solrindexer;
/*
 * BioModels Database
 *
 * This is the code of BioModels Database
 * Copyright (C) BioModels.net  2005-2015
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import org.apache.log4j.Logger;

/**
 * Represents a data collection from the (MIRIAM) Registry (and *not* a resource).
 * This is populated from the XML export from the Registry and Constants.RESOURCES_FOR_ANNOTATION.
 *
 * @see net.biomodels.jummp.annotationstore.ResourceReference
 *
 * @author Camille Laibe, Chen Li
 */
public final class Resource implements Comparable<Resource>
{
private static final Logger logger = Logger.getLogger(Resource.class);
    private String datatypeId;
    private String name;
    private String idPattern;
    // official URI, so it should always be the unique according to the MIRIAM Registry (currently set to the URN form by MIRIAMDatabase)
    private String uri;
    private String location;
    private String urlPrefix;
    private String urlSuffix;
    private String idInputHelper;

    public int compareTo(Resource that) {
        int difference = this.name.compareTo(that.name);
        if (difference < 0) {
            return -1;
        } else if (difference > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object other) {
        if (null == other || getClass() != other.getClass()) {
            return false;
        }
        if (other instanceof Resource) {
            return this.name.equals(((Resource) other).name);
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = 37;
        return result * this.name.hashCode();
    }

    /**
     * Retrieves the identifier of the data collection.
     * @return
     */
    public String getDatatypeId()
    {
        return this.datatypeId;
    }

    /**
     * Retrieves the name of the resource.
     * @return
     */
    public String getName()
    {
        return name;
    }

    /**
     * Retrieves the URL to access one piece of data knowing its accession.
     * @param accession
     * @return
     */
    public String getAction(String accession)
    {
        String action_id = null;

        action_id = accession;

        // remove the following code when all the Reactome id become stable.
        if (this.name.equalsIgnoreCase("Reactome") && !(action_id.startsWith("REACT_"))) {
            return "http://www.reactome.org/cgi-bin/eventbrowser_st_id?FROM_REACTOME=1&ST_ID=" + action_id;
        }
        // remove the upper code when all the Reactome id become stable.

        return this.urlPrefix + action_id + this.urlSuffix;
    }

    /**
     * Retrieves the URL of the website of the resource.
     * @return
     */
    public String getLocation()
    {
        return this.location;
    }

    /**
     * Separates a MIRIAM URI into an data collection (everything before the identifier) and identifier parts.
     */
    public static String[] parseURI(String rdfResource, boolean check) {
        Boolean URL = false;   // official URL form
        String uri = null;
        int separator_index;

        if (rdfResource.toLowerCase().startsWith("urn:miriam:"))
        {
            separator_index = 11 + rdfResource.substring(11).indexOf(":");
            uri = rdfResource.substring(0, separator_index);
        }
        else if (rdfResource.toLowerCase().startsWith("http://identifiers.org/"))
        {
            separator_index = 23 + rdfResource.substring(23).indexOf("/");
            uri = rdfResource.substring(0, separator_index) + "/";   // we need the final '/' as it is recorded as such in the Registry
            URL = true;
        }
        else {
            separator_index = rdfResource.lastIndexOf('#');
            if (separator_index > 0) {
                uri = rdfResource.substring(0, separator_index);
            } else {
                // non MIRIAM URI (case handled below)
            }
        }

        if (separator_index == -1)
        {
            logger.warn("Unable to process the following annotation: " + rdfResource);
            return null;
        }

        String[] response = new String[2];
        response[0] = uri;
        try {
            if (!URL) {
                response[1] = URLDecoder.decode(rdfResource.substring(separator_index + 1, rdfResource.length()), "UTF-8");
            } else {
                response[1] = rdfResource.substring(separator_index + 1, rdfResource.length());   // no encoding with the official URL form
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }

        return response;
    }

    /**
     * Retrieves the "input helper" (the beginning of the regular expression which describe the non changing part of the identifier).
     * @return
     */
    protected String getIdInputHelper()
    {
        return this.idInputHelper;
    }

    @Override
    public String toString()
    {
        StringBuilder result = new StringBuilder();
        String[] fields = new String[] {datatypeId, name, idPattern, uri, location,
            urlPrefix, urlSuffix};
        for (int i = 0; i < fields.length; i++) {
            result.append(fields[i]);
                if (i <= fields.length - 1) {
                    result.append('\t');
                }
        }
        return result.toString();
    }

    public void setDatatypeId(String datatypeId) {
        this.datatypeId = datatypeId;
    }

    public void setIdPattern(String idPattern) {
        this.idPattern = idPattern;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setUrlPrefix(String urlPrefix) {
        this.urlPrefix = urlPrefix;
    }

    public void setUrlSuffix(String urlSuffix) {
        this.urlSuffix = urlSuffix;
    }

    public void setIdInputHelper(String idInputHelper) {
        this.idInputHelper = idInputHelper;
    }

    public void setName(String name) {
        this.name = name.equals("GO") ? "Gene Ontology" : name;
    }
}
