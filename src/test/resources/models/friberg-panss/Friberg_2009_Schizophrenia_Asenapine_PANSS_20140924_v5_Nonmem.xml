<?xml version="1.0" encoding="UTF-8"?>
<PharmML xmlns="http://www.pharmml.org/2013/03/PharmML"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.pharmml.org/2013/03/PharmML http://www.pharmml.org/2013/03/PharmML"     
    xmlns:ct="http://www.pharmml.org/2013/03/CommonTypes"
    writtenVersion="0.3.1">
    
    <Name id="id1" xmlns="http://www.pharmml.org/2013/03/CommonTypes">Friberg_2009_Schizophrenia_Asenapine_PANSS</Name>
    
    <Description id="id2" xmlns="http://www.pharmml.org/2013/03/CommonTypes"> 	  
        Modeling and simulation of the time course of asenapine exposure response and dropout patterns in acute schizophrenia.
        Friberg LE, de Greef R, Kerbusch T, Karlsson MO
        Clinical pharmacology and therapeutics, 7/2009, Volume 86, Issue 1, pages: 84-91
    </Description>
    
    <!-- INDEPENDENT VARIABLE -->
    <IndependentVariable id="id3" symbId="t"/>
          
       
    <!-- SYMBOL DEFINITION - RESUIDUAL ERROR MODEL, i.e. additive model here -->
    <ct:FunctionDefinition symbId="additiveErrorModel" symbolType="real">
        <ct:FunctionArgument symbId="a" symbolType="real"/>
        <ct:Definition>
            <ct:SymbRef symbIdRef="a"/>
        </ct:Definition>
    </ct:FunctionDefinition>
    
    
    <!-- MODEL DEFINITION -->
    <ModelDefinition xmlns="http://www.pharmml.org/2013/03/ModelDefinition">
           
        <!-- VARIABILITY MODEL -->
            <!-- variability model for the random effects -->
            <VariabilityModel blkId="model" type='parameterVariability'>
                <Level symbId="indiv"/>
            </VariabilityModel>
        
            <!-- variability model for residual error -->
            <VariabilityModel blkId="obsErr" type='residualError'>
                <Level symbId="residual"/>
            </VariabilityModel>
        
        <!-- COVARIATE MODEL -->
        <CovariateModel blkId="c1">
            <Covariate symbId="DDUR">
                <ct:Description> Duration of the present episode </ct:Description>
                <Categorical>
                    <Category catId="1">
                        <ct:Name>Less than 1 week</ct:Name>
                    </Category>
                    <Category catId="2">
                        <ct:Name>Between 2 and 4 weeks</ct:Name>
                    </Category>
                    <Category catId="3">
                        <ct:Name>Between 1 and 6 months</ct:Name>
                    </Category>
                    <Category catId="4">
                        <ct:Name>Between 6 and 12 months</ct:Name>
                    </Category>
                    <Category catId="5">
                        <ct:Name>More than 12 months</ct:Name>
                    </Category>
                </Categorical> 
            </Covariate>
            <Covariate symbId="STUD">
                <ct:Description> Study number: first digit indicating whether the study was phase II or phase III, second digit indication substudy </ct:Description>
                <Categorical>
                    <Category catId="21">
                        <ct:Name>STUDY 21</ct:Name>
                    </Category>
                    <Category catId="22">
                        <ct:Name>STUDY 22</ct:Name>
                    </Category>
                    <Category catId="23">
                        <ct:Name>STUDY 23</ct:Name>
                    </Category>
                    <Category catId="31">
                        <ct:Name>STUDY 31</ct:Name>
                    </Category>
                    <Category catId="32">
                        <ct:Name>STUDY 32</ct:Name>
                    </Category>
                    <Category catId="33">
                        <ct:Name>STUDY 33</ct:Name>
                    </Category>
                </Categorical>
            </Covariate>
            <Covariate symbId="US">
                <ct:Description> Study performed in the US (no = 0, yes = 1) </ct:Description>
                <Categorical>
                    <Category catId="1">
                        <ct:Name>US</ct:Name>
                    </Category>
                    <Category catId="0">
                        <ct:Name>NON_US</ct:Name>
                    </Category>
                </Categorical>
            </Covariate>
            <Covariate symbId="HOSP">
                <ct:Description> Patient hospitalized at time of observation (no = 0, yes = 1) </ct:Description>
                <Categorical>
                    <Category catId="1">
                        <ct:Name>Inpatient</ct:Name>
                    </Category>
                    <Category catId="0">
                        <ct:Name>Outpatient</ct:Name>
                    </Category>
                </Categorical>
            </Covariate>               
        </CovariateModel>
        
        <!-- PARAMETER MODEL -->
        <ParameterModel blkId="pm1">
            
            <!-- Define DDU variables depending upon episode duration (DDUR): 0 for less than 1 month, 1 otherwise-->
            <SimpleParameter symbId="DDU">
                <ct:Assign>
                    <Equation xmlns="http://www.pharmml.org/2013/03/Maths">
                        <Piecewise>
                            <Piece>
                                <ct:Int>1</ct:Int>
                                <Condition>
                                    <LogicBinop op="gt">
                                        <ct:SymbRef blkIdRef="c1" symbIdRef="DDUR"/>
                                        <ct:Int>2</ct:Int>
                                    </LogicBinop>
                                </Condition>
                            </Piece>
                            <Piece>
                                <ct:Int>0</ct:Int>
                                <Condition>
                                    <Otherwise/>
                                </Condition>
                            </Piece>
                        </Piecewise>
                    </Equation>
                </ct:Assign>
            </SimpleParameter>
            
            <!-- Define PHASE variables for the different clinical trials (STUD): 0 for phaseII and 1 for phaseIII-->
            <SimpleParameter symbId="PHASE">
                <ct:Assign>
                    <Equation xmlns="http://www.pharmml.org/2013/03/Maths">
                        <Piecewise>
                            <Piece>
                                <ct:Int>1</ct:Int>
                                <Condition>
                                    <LogicBinop op="gt">
                                        <ct:SymbRef blkIdRef="c1" symbIdRef="STUD"/>
                                        <ct:Int>30</ct:Int>
                                    </LogicBinop>
                                </Condition>
                            </Piece>
                            <Piece>
                                <ct:Int>0</ct:Int>
                                <Condition>
                                    <Otherwise/>
                                </Condition>
                            </Piece>
                        </Piecewise>
                    </Equation>
                </ct:Assign>
            </SimpleParameter>         
            
            
            <!-- PLACEBO EFFECT MODEL PARAMETERS -->
            <!-- PAN0-CHRON : proportional difference in PAN0 for chronic patiens -->
            <SimpleParameter symbId="PAN0_CHRON" id="id4"/>
            <!-- PAN0 : baseline PANSS score-->
            <SimpleParameter symbId="PAN0_II" id="id5"/>
            <SimpleParameter symbId="PAN0_III" id="id6"/>
            <SimpleParameter symbId="pop_PAN0" id="id7">
                <ct:Assign>
                    <Equation xmlns="http://www.pharmml.org/2013/03/Maths">
                        <Piecewise>
                            <Piece>
                                <Binop op="times">
                                    <ct:SymbRef symbIdRef="PAN0_II"/>
                                    <Binop op="plus">
                                        <ct:Real>1</ct:Real>
                                        <Binop op="times">
                                            <ct:SymbRef symbIdRef="PAN0_CHRON"/>
                                            <ct:SymbRef blkIdRef="c1" symbIdRef="DDU"/>
                                        </Binop>
                                    </Binop>
                                </Binop>                                   
                                <Condition>
                                    <LogicBinop op="eq">
                                        <ct:SymbRef blkIdRef="c1" symbIdRef="PHASE"/>
                                        <ct:Int>0</ct:Int>
                                    </LogicBinop>
                                </Condition>
                            </Piece>
                            <Piece>
                                <Binop op="times">
                                    <ct:SymbRef symbIdRef="PAN0_III"/>
                                    <Binop op="plus">
                                        <ct:Real>1</ct:Real>
                                        <Binop op="times">
                                            <ct:SymbRef symbIdRef="PAN0_CHRON"/>
                                            <ct:SymbRef blkIdRef="c1" symbIdRef="DDU"/>
                                        </Binop>
                                    </Binop>
                                </Binop>                                   
                                <Condition>
                                    <LogicBinop op="eq">
                                        <ct:SymbRef blkIdRef="c1" symbIdRef="PHASE"/>
                                        <ct:Int>1</ct:Int>
                                    </LogicBinop>
                                </Condition>
                            </Piece>                                
                        </Piecewise>
                    </Equation>
                </ct:Assign>
            </SimpleParameter>                
            <SimpleParameter symbId="omega_PAN0" id="id8"/>
            <RandomVariable symbId="eta_PAN0" id="id9">
                <ct:VariabilityReference>
                    <ct:SymbRef blkIdRef="model" symbIdRef="indiv"/>
                </ct:VariabilityReference>
                <NormalDistribution xmlns="http://www.uncertml.org/3.0" 
                    definition="http://www.uncertml.org/distributions/normal">
                    <mean><rVal>0</rVal></mean>
                    <variance><var varId="omega_PAN0"/></variance>
                </NormalDistribution>
            </RandomVariable>
            <IndividualParameter symbId="PAN0" id="i10">
                <GaussianModel>
                    <Transformation>identity</Transformation>
                    <GeneralCovariate>
                        <ct:Assign>
                            <ct:SymbRef symbIdRef="pop_PAN0"/>
                        </ct:Assign>
                    </GeneralCovariate>
                    <RandomEffects>
                        <ct:SymbRef symbIdRef="eta_PAN0"/>
                    </RandomEffects>
                </GaussianModel>
            </IndividualParameter>      

            <!-- PMAX_PHASE: Proportional diiference in PMAX for patients in phase III -->
            <SimpleParameter symbId="PMAX_PHASEIII" id="id11"/>
            <!-- PMAX: Maximum placebo effect -->
            <SimpleParameter symbId="TVPMAX" id="id12"/>
            <SimpleParameter symbId="pop_PMAX" id="id13">
                <ct:Assign>
                    <Equation xmlns="http://www.pharmml.org/2013/03/Maths">
                        <Binop op="times">
                            <ct:SymbRef symbIdRef="TVPMAX"/>
                            <Binop op="plus">
                                <ct:Real>1</ct:Real>
                                <Binop op="times">
                                    <ct:SymbRef symbIdRef="PMAX_PHASEIII"/>
                                    <ct:SymbRef blkIdRef="c1" symbIdRef="PHASE"/>
                                </Binop>
                            </Binop>
                        </Binop>
                    </Equation>
                </ct:Assign>
            </SimpleParameter>
            <SimpleParameter symbId="omega_PMAX" id="id14"/>
            <RandomVariable symbId="eta_PMAX" id="id15">
                <ct:VariabilityReference>
                    <ct:SymbRef blkIdRef="model" symbIdRef="indiv"/>
                </ct:VariabilityReference>
                <NormalDistribution xmlns="http://www.uncertml.org/3.0" definition="http://www.uncertml.org/distributions/normal">
                    <mean>
                        <rVal>0</rVal>
                    </mean>
                    <variance>
                        <var varId="omega_PMAX"/>
                    </variance>
                </NormalDistribution>
            </RandomVariable>
            <IndividualParameter symbId="PMAX">
                <GaussianModel>
                    <Transformation>identity</Transformation>
                    <GeneralCovariate>
                        <ct:Assign>
                            <ct:SymbRef symbIdRef="pop_PMAX"/>                           
                        </ct:Assign>
                    </GeneralCovariate>
                    <RandomEffects>
                        <ct:SymbRef symbIdRef="eta_PMAX"/>
                    </RandomEffects>
                </GaussianModel>
            </IndividualParameter>

            <!-- TP: Time to achieve PMAX -->
            <SimpleParameter symbId="TP" id="id16"/>
                        
            <!-- POW: Power parameter in Weibull equation  -->
            <SimpleParameter symbId="POW" id="id17"/>



            <!-- ASENAPINE EFFECT MODEL PARAMETERS -->
            <!-- EMAX : Maximum asenapine effect at day42 -->
            <SimpleParameter symbId="EMAX" id="id18"/>
                 
            <!-- AUC50: Asenapine AUC to achieve half of the EMAX -->
            <SimpleParameter symbId="pop_AUC50" id="id19"/>
            <SimpleParameter symbId="omega_AUC50" id="id20"/>
            <RandomVariable symbId="eta_AUC50" id="id21">
                <ct:VariabilityReference>
                    <ct:SymbRef blkIdRef="model" symbIdRef="indiv"/>
                </ct:VariabilityReference>
                <NormalDistribution xmlns="http://www.uncertml.org/3.0" definition="http://www.uncertml.org/distributions/normal">
                    <mean>
                        <rVal>0</rVal>
                    </mean>
                    <variance>
                        <var varId="omega_AUC50"/>
                    </variance>
                </NormalDistribution>
            </RandomVariable>
            <IndividualParameter symbId="AUC50">
                <GaussianModel>
                    <Transformation>log</Transformation>
                    <LinearCovariate>
                        <PopulationParameter>
                            <ct:Assign>
                                <ct:SymbRef symbIdRef="pop_AUC50"/>
                            </ct:Assign>
                        </PopulationParameter>
                    </LinearCovariate>
                    <RandomEffects>
                        <ct:SymbRef symbIdRef="eta_AUC50"/>
                    </RandomEffects>
                </GaussianModel>
            </IndividualParameter>
            
            <!-- Residual error  -->
            <!-- Proportional difference in residual error for non-hospitalized patients -->
            <SimpleParameter symbId="THETA_HOSP"/>            
            <SimpleParameter symbId="CHOSP">
                <ct:Assign>
                    <Equation xmlns="http://www.pharmml.org/2013/03/Maths">
                        <Piecewise>
                            <Piece>
                                <ct:Real>1</ct:Real>
                                <Condition>
                                    <LogicBinop op="neq">
                                        <ct:SymbRef blkIdRef="c1" symbIdRef="HOSP"/>
                                        <ct:Real>0</ct:Real>
                                    </LogicBinop>
                                </Condition>
                            </Piece>
                            <Piece>
                                <Binop op="plus">
                                    <ct:Real>1</ct:Real>
                                    <ct:SymbRef symbIdRef="THETA_HOSP"></ct:SymbRef>
                                </Binop>
                                <Condition>
                                    <LogicBinop op="eq">
                                        <ct:SymbRef blkIdRef="c1" symbIdRef="HOSP"/>
                                        <ct:Real>0</ct:Real>
                                    </LogicBinop>
                                </Condition>
                            </Piece>
                        </Piecewise>
                    </Equation>
                </ct:Assign>
            </SimpleParameter>

            <!-- Proportional difference in residual error for patients in the US -->
            <SimpleParameter symbId="THETA_US"/>

            <!-- Tipical residual error -->
            <SimpleParameter symbId="pop_error"/>
            
            <!-- Define the covariates and IIV in the residual error -->
            <SimpleParameter symbId="pop_W">
                <ct:Assign>
                    <Equation xmlns="http://www.pharmml.org/2013/03/Maths">
                        <Binop op="times">
                            <ct:SymbRef symbIdRef="CHOSP"></ct:SymbRef>
                            <Binop op="times">
                                <ct:SymbRef symbIdRef="pop_error"/>
                                <Binop op="plus">
                                    <ct:Real>1</ct:Real>
                                    <Binop op="times">
                                        <ct:SymbRef symbIdRef="THETA_US"/>
                                        <ct:SymbRef blkIdRef="c1" symbIdRef="US"/>
                                    </Binop>
                                </Binop>
                            </Binop>
                        </Binop>
                    </Equation>
                </ct:Assign>
            </SimpleParameter>
            <SimpleParameter symbId="omega_W"/>
            <RandomVariable symbId="eta_W">
                <ct:VariabilityReference>
                    <ct:SymbRef blkIdRef="model" symbIdRef="indiv"/>
                </ct:VariabilityReference>
                <NormalDistribution xmlns="http://www.uncertml.org/3.0" 
                    definition="http://www.uncertml.org/distributions/normal">
                    <mean><rVal>0</rVal></mean>
                    <variance><var varId="omega_W"/></variance>
                </NormalDistribution>            
            </RandomVariable>
            <IndividualParameter symbId="W">
                <GaussianModel>
                    <Transformation>log</Transformation>
                    <LinearCovariate>
                        <PopulationParameter>
                            <ct:Assign>
                                <ct:SymbRef symbIdRef="pop_W"/>
                            </ct:Assign>
                        </PopulationParameter>
                    </LinearCovariate>
                    <RandomEffects>
                        <ct:SymbRef symbIdRef="eta_W"/>
                    </RandomEffects>
                </GaussianModel>
            </IndividualParameter>
            
            
            <!-- Correlation between IIV in PAN0 and IIV in PMAX -->
            <SimpleParameter symbId="cov_PAN0_PMAX" id="id22"/>
            <Correlation>
                <ct:VariabilityReference>
                    <ct:SymbRef blkIdRef="model" symbIdRef="indiv"/>
                </ct:VariabilityReference>
                <Pairwise>
                    <RandomVariable1>
                        <ct:SymbRef symbIdRef="eta_PAN0"/>
                    </RandomVariable1>
                    <RandomVariable2>
                        <ct:SymbRef symbIdRef="eta_PMAX"/>
                    </RandomVariable2>
                    <Covariance>
                        <ct:SymbRef symbIdRef="cov_PAN0_PMAX"/>
                    </Covariance>
                </Pairwise>          
            </Correlation>         

        </ParameterModel>



              
        <!-- STRUCTURAL MODEL -->
        <StructuralModel blkId="sm1">
           
           <!-- Placebo model -->
            <!-- exponential term -->
            <ct:Variable symbolType="real" symbId="expterm">
                <ct:Assign>
                    <Equation xmlns="http://www.pharmml.org/2013/03/Maths">
                        <Uniop op="exp">
                            <Uniop op="minus">
                                <Binop op="power">
                                    <Binop op="divide">
                                        <ct:SymbRef symbIdRef="t"/>
                                        <ct:SymbRef blkIdRef="pm1" symbIdRef="TP"/>
                                    </Binop>
                                    <ct:SymbRef blkIdRef="pm1" symbIdRef="POW"/>
                                </Binop>
                            </Uniop>
                        </Uniop>
                    </Equation>
                </ct:Assign>                
            </ct:Variable>                   
            <ct:Variable symbolType="real" symbId="PMOD" id="id23">
                <ct:Assign>
                    <Equation xmlns="http://www.pharmml.org/2013/03/Maths">
                        <Binop op="times">
                            <ct:SymbRef blkIdRef="pm1" symbIdRef="PMAX"/>
                            <Binop op="minus">
                                <ct:Real>1</ct:Real>
                                <ct:SymbRef symbIdRef="expterm"/>
                            </Binop>
                        </Binop>
                    </Equation>
                </ct:Assign>
            </ct:Variable>
            
            <!-- Asenapine model -->
            <!-- FT: time course parameter for Asenapine model -->
            <ct:Variable symbolType="real" symbId="FT">
                <ct:Assign>
                    <Equation xmlns="http://www.pharmml.org/2013/03/Maths">
                        <Piecewise>
                            <Piece>
                                <ct:Int>1</ct:Int>
                                <Condition>
                                    <LogicBinop op="gt">
                                        <ct:SymbRef symbIdRef="t"/>
                                        <ct:Int>42</ct:Int>
                                    </LogicBinop>
                                </Condition>
                            </Piece>
                            <Piece>
                                <Binop op="divide">
                                    <ct:SymbRef symbIdRef="t"/>
                                    <ct:Real>42</ct:Real>
                                </Binop>                               
                                <Condition>
                                    <Otherwise/>
                                </Condition>
                            </Piece>
                        </Piecewise>                       
                    </Equation>
                </ct:Assign>
            </ct:Variable>
             <!-- AUC: input -->
            <ct:Variable symbolType="real" symbId="AUC">
                <ct:Assign>
                    <ct:Interpolation>
                        <ct:Algorithm>constant</ct:Algorithm>
                        <ct:InterpIndepVar>
                            <ct:SymbRef symbIdRef="t"></ct:SymbRef>
                        </ct:InterpIndepVar>
                    </ct:Interpolation>
                </ct:Assign>
            </ct:Variable>         
            <!-- Asenapine effect -->
            <ct:Variable symbolType="real" symbId="EFF" id="id24">
                <ct:Assign>
                    <Equation xmlns="http://www.pharmml.org/2013/03/Maths">
                        <Binop op="times">
                            <Binop op="divide">
                                <Binop op="times">
                                    <ct:SymbRef blkIdRef="pm1" symbIdRef="EMAX"/>
                                    <ct:SymbRef symbIdRef="AUC"/>
                                </Binop>
                                <Binop op="plus">
                                    <ct:SymbRef blkIdRef="pm1" symbIdRef="AUC50"/>
                                    <ct:SymbRef symbIdRef="AUC"/>
                                </Binop>
                            </Binop>
                            <ct:SymbRef symbIdRef="FT"/>
                        </Binop>
                    </Equation>
                </ct:Assign>
            </ct:Variable>        
            <!-- Define EMOD: equal to the drug effect if AUC>0 and Time>0; 0 otherwise -->     
            <ct:Variable symbolType="real" symbId="EMOD">
                <ct:Assign>
                    <Equation xmlns="http://www.pharmml.org/2013/03/Maths">
                        <Piecewise>
                            <Piece>
                                <ct:SymbRef symbIdRef="EFF"></ct:SymbRef>
                                <Condition>
                                    <LogicBinop op="and">
                                        <LogicBinop op="gt">
                                            <ct:SymbRef symbIdRef="t"/>
                                            <ct:Real>0</ct:Real>
                                        </LogicBinop>
                                        <LogicBinop op="gt">
                                            <ct:SymbRef symbIdRef="AUC"/>
                                            <ct:Real>0</ct:Real>
                                        </LogicBinop>                                       
                                    </LogicBinop>                                   
                                </Condition>
                            </Piece>
                            <Piece>
                                <ct:Real>0</ct:Real>
                                <Condition>
                                    <Otherwise/>
                                </Condition>
                            </Piece>
                        </Piecewise>
                    </Equation>
                </ct:Assign>
            </ct:Variable>
            
            <!-- Total PANSS score -->
            <ct:Variable symbolType="real" symbId="PANSS_total" id="id25">
               <ct:Assign>
                   <Equation xmlns="http://www.pharmml.org/2013/03/Maths">
                       <Binop op="times">
                           <ct:SymbRef blkIdRef="pm1" symbIdRef="PAN0"/>
                           <Binop op="times">
                               <Binop op="minus">
                                   <ct:Real>1</ct:Real>
                                   <ct:SymbRef symbIdRef="PMOD"/>
                               </Binop>
                               <Binop op="minus">
                                   <ct:Real>1</ct:Real>
                                   <ct:SymbRef symbIdRef="EMOD"/>
                               </Binop>
                           </Binop>
                       </Binop>
                   </Equation>
               </ct:Assign>
           </ct:Variable>
        </StructuralModel>
        
        
        
        <!-- OBSERVATION MODEL -->
        <ObservationModel blkId="om1">
            <RandomVariable symbId="eps">
                <ct:VariabilityReference>
                    <ct:SymbRef blkIdRef="obsErr" symbIdRef="residual"/>
                </ct:VariabilityReference>
                <NormalDistribution xmlns="http://www.uncertml.org/3.0" definition="http://www.uncertml.org/distributions/normal">
                    <mean>
                        <rVal>0</rVal>
                    </mean>
                    <variance>
                        <prVal>1</prVal>
                    </variance>
                </NormalDistribution>
            </RandomVariable>
            <Standard symbId="PANSS_total_obs">
                <Output>
                    <ct:SymbRef blkIdRef="sm1" symbIdRef="PANSS_total"/>
                </Output>
                <ErrorModel>
                    <ct:Assign>
                        <Equation xmlns="http://www.pharmml.org/2013/03/Maths">
                            <FunctionCall>
                                <ct:SymbRef symbIdRef="AdditiveErrorModel"/>
                                <FunctionArgument symbId="a">
                                    <ct:SymbRef blkIdRef="pm1" symbIdRef="W"/>
                                </FunctionArgument>
                            </FunctionCall>
                        </Equation>
                    </ct:Assign>
                </ErrorModel>
                <ResidualError>
                    <ct:SymbRef symbIdRef="eps"></ct:SymbRef>
                </ResidualError>
            </Standard>
        </ObservationModel>
    </ModelDefinition>     
    

    <!-- BLOCK II: TRIAL DEFINITION -->
 
        

    <!-- BLOCK III: MODELLING STEPS -->
    <ModellingSteps xmlns="http://www.pharmml.org/2013/03/ModellingSteps">                           
             
        <!-- NONMEM dataset -->     
        <NONMEMdataSet oid="NMoid">
            <ColumnMapping>
                <ColumnRef xmlns="http://www.pharmml.org/2013/08/Dataset" columnIdRef="TIME"></ColumnRef>
                <ct:SymbRef symbIdRef="t"></ct:SymbRef>
            </ColumnMapping>
            <ColumnMapping>
                <ColumnRef xmlns="http://www.pharmml.org/2013/08/Dataset" columnIdRef="DV"></ColumnRef>
                <ct:SymbRef blkIdRef="om1" symbIdRef="PANSS_total_obs"></ct:SymbRef>
            </ColumnMapping>
            <ColumnMapping>
                <ColumnRef xmlns="http://www.pharmml.org/2013/08/Dataset" columnIdRef="STUD"/>
                <ct:SymbRef blkIdRef="c1" symbIdRef="STUD"/>
            </ColumnMapping>
            <ColumnMapping>
                <ColumnRef xmlns="http://www.pharmml.org/2013/08/Dataset" columnIdRef="DDUR"/>
                <ct:SymbRef blkIdRef="c1" symbIdRef="DDUR"/>
            </ColumnMapping>
            <ColumnMapping>
                <ColumnRef xmlns="http://www.pharmml.org/2013/08/Dataset" columnIdRef="US"/>
                <ct:SymbRef blkIdRef="c1" symbIdRef="US"/>
            </ColumnMapping>
            <ColumnMapping>
                <ColumnRef xmlns="http://www.pharmml.org/2013/08/Dataset" columnIdRef="HOSP"/>
                <ct:SymbRef blkIdRef="c1" symbIdRef="HOSP"/>
            </ColumnMapping>
            <ColumnMapping>
                <ColumnRef xmlns="http://www.pharmml.org/2013/08/Dataset" columnIdRef="AUC"></ColumnRef>
                <ct:SymbRef blkIdRef="sm1" symbIdRef="AUC"></ct:SymbRef>
            </ColumnMapping>
            <DataSet xmlns="http://www.pharmml.org/2013/08/Dataset">
                <Definition>
                    <Column columnId="ID" columnType="id" valueType="id" columnNum="1"/> 
                    <Column columnId="TIME" columnType="idv" valueType="int" columnNum="2"/> 
                    <Column columnId="DV" columnType="dv" valueType="int" columnNum="3"/> 
                    <Column columnId="STUD" columnType="covariate" valueType="int" columnNum="4"/> 
                    <Column columnId="DDUR" columnType="covariate" valueType="int" columnNum="5"/> 
                    <Column columnId="HOSP" columnType="covariate" valueType="int" columnNum="6"/>                           
                    <Column columnId="US" columnType="covariate" valueType="int" columnNum="7"/>
                    <Column columnId="AUC" columnType="covariate" valueType="real" columnNum="8"/>
                    <Column columnId="MDV" columnType="mdv" valueType="real" columnNum="9"/>
                </Definition>
                <ImportData oid="importobs">                             
                    <path>PANSS_Friberg2009_simdata_2.csv</path>
                    <format>CSV</format>
                    <delimiter>COMMA</delimiter>
                </ImportData>
            </DataSet>
        </NONMEMdataSet>
        
        
        <!-- ESTIMATION STEP -->
        <EstimationStep oid="estTask1">                            
            <ParametersToEstimate>
                <!-- INITIAL CONDITIONS -->
                <!-- PAN0 -->
                <ParameterEstimation>
                    <ct:SymbRef blkIdRef="pm1" symbIdRef="PAN0_CHRON"/>
                    <InitialEstimate>
                        <ct:Real>-0.0339</ct:Real>
                    </InitialEstimate>
                </ParameterEstimation>
                <ParameterEstimation>
                    <ct:SymbRef blkIdRef="pm1" symbIdRef="PAN0_II"/>
                    <InitialEstimate>
                        <ct:Real>94.0</ct:Real>
                    </InitialEstimate>
                </ParameterEstimation>
                <ParameterEstimation>
                    <ct:SymbRef blkIdRef="pm1" symbIdRef="PAN0_III"/>
                    <InitialEstimate>
                        <ct:Real>90.5</ct:Real>
                    </InitialEstimate>
                </ParameterEstimation>
                <ParameterEstimation>
                    <ct:SymbRef blkIdRef="pm1" symbIdRef="omega_PAN0"/>
                    <InitialEstimate>
                        <ct:Real>167</ct:Real>
                    </InitialEstimate>
                </ParameterEstimation>

                <!-- PMAX -->
                <ParameterEstimation>
                    <ct:SymbRef blkIdRef="pm1" symbIdRef="PMAX_PHASEIII"/>
                    <InitialEstimate>
                        <ct:Real>0.688</ct:Real>
                    </InitialEstimate>
                </ParameterEstimation>
                <ParameterEstimation>
                    <ct:SymbRef blkIdRef="pm1" symbIdRef="TVPMAX"/>
                    <InitialEstimate>
                        <ct:Real>0.0859</ct:Real>
                    </InitialEstimate>
                </ParameterEstimation>
                <ParameterEstimation>
                    <ct:SymbRef blkIdRef="pm1" symbIdRef="omega_PMAX"/>
                    <InitialEstimate>
                        <ct:Real>0.0249</ct:Real>
                    </InitialEstimate>
                </ParameterEstimation>
                
                <!-- TP -->
                <ParameterEstimation>
                    <ct:SymbRef blkIdRef="pm1" symbIdRef="TP"/>
                    <InitialEstimate>
                        <ct:Real>13.2</ct:Real>
                    </InitialEstimate>
                    <LowerBound>
                        <ct:Int>0</ct:Int>
                    </LowerBound>
                </ParameterEstimation>

                <!-- POW -->
                <ParameterEstimation>
                    <ct:SymbRef blkIdRef="pm1" symbIdRef="POW"/>
                    <InitialEstimate>
                        <ct:Real>1.24</ct:Real>
                    </InitialEstimate>
                    <LowerBound>
                        <ct:Int>0</ct:Int>
                    </LowerBound>
                </ParameterEstimation>
                
                <!-- EMAX -->
                <ParameterEstimation>
                    <ct:SymbRef blkIdRef="pm1" symbIdRef="EMAX"/>
                    <InitialEstimate>
                        <ct:Real>0.191</ct:Real>
                    </InitialEstimate>
                </ParameterEstimation>
                
                <!-- AUC50 -->
                <ParameterEstimation>
                    <ct:SymbRef blkIdRef="pm1" symbIdRef="pop_AUC50"/>
                    <InitialEstimate>
                        <ct:Real>82</ct:Real>
                    </InitialEstimate>
                </ParameterEstimation>
                <ParameterEstimation>
                    <ct:SymbRef blkIdRef="pm1" symbIdRef="omega_AUC50"/>
                    <InitialEstimate>
                        <ct:Real>21.7</ct:Real>
                    </InitialEstimate>
                </ParameterEstimation>                

                <!-- Correlation PAN0_PMAX -->
                <ParameterEstimation>
                    <ct:SymbRef blkIdRef="pm1" symbIdRef="cov_PAN0_PMAX"/>
                    <InitialEstimate>
                        <ct:Real>-0.395</ct:Real>
                    </InitialEstimate>
                </ParameterEstimation>

                <!-- RESIDUAL ERROR PARAMETER -->
                <!-- W -->
                <ParameterEstimation>
                    <ct:SymbRef blkIdRef="pm1" symbIdRef="pop_error"/>
                    <InitialEstimate>
                        <ct:Real>3.52</ct:Real>
                    </InitialEstimate>
                </ParameterEstimation>
                
                <!-- Proportional difference in residual error for non-hospitalized patients -->
                <ParameterEstimation>
                    <ct:SymbRef blkIdRef="pm1" symbIdRef="THETA_HOSP"/>
                    <InitialEstimate>
                        <ct:Real>-0.145</ct:Real>
                    </InitialEstimate>
                </ParameterEstimation>  
                
                <!-- Proportional difference in residual error for patients in the US -->
                <ParameterEstimation>
                    <ct:SymbRef blkIdRef="pm1" symbIdRef="THETA_US"/>
                    <InitialEstimate>
                        <ct:Real>0.623</ct:Real>
                    </InitialEstimate>
                </ParameterEstimation>

                <!-- IIV residual error  -->
                <ParameterEstimation>
                    <ct:SymbRef blkIdRef="pm1" symbIdRef="omega_W"/>
                    <InitialEstimate>
                       <ct:Real>0.196</ct:Real>
                    </InitialEstimate>
                </ParameterEstimation>               
                
            </ParametersToEstimate>
            <Operation order="1" opType="estPop"/>
            <Operation order="2" opType="estIndiv"/>
            
        </EstimationStep>    
 
        <!-- STEP DEPENDENCIES -->
        <StepDependencies>
            <Step>
                <ct:OidRef oidRef="estTask1"/>
            </Step>
        </StepDependencies>
        
    </ModellingSteps> 
    
</PharmML>