package net.biomodels.jummp.indexing.solrindexer.miriam

import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

import static org.junit.Assert.*

/**
 * @author Mihai Glonț <mglont@ebi.ac.uk>
 */
@RunWith(JUnit4.class)
class MiriamRegistryServiceTest {
    @Test
    void nullURIsAreRejected() {
        String uri = null
        assertNull MiriamRegistryService.updateURI(uri)
        uri = ""
        assertEquals(uri, MiriamRegistryService.updateURI(uri))
        uri = "    "
        assertEquals(uri, MiriamRegistryService.updateURI(uri))
    }

    @Test
    void deprecatedURIsAreUpdated() {
        assertNull MiriamRegistryService.updateURI("http://identifiers.org/foo/bar:123")
        String uri = "http://identifiers.org/obo.bto/BTO:0000316"
        String expected = "http://identifiers.org/bto/BTO:0000316"
        assertEquals(expected, MiriamRegistryService.updateURI(uri))
        expected = "http://identifiers.org/chebi/CHEBI:28445"
        assertEquals(expected, MiriamRegistryService.updateURI(expected))
    }
}
